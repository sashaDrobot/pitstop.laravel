<?php

/*Route::post('/upload', function(Request $request) {
        $request->file('file')->storeAs('public/upload', $request->file('file')->getClientOriginalName());
    });*/

Route::get('/', 'MainController@index');
Route::get('/getMainFormData', 'MainController@getMainFormData');
Route::post('/getModelCar', 'MainController@getModelCar');
Route::post('/getYearCar', 'MainController@getYearCar');
Route::post('/getModificationCar', 'MainController@getModificationCar');
Route::post('/getTiresByBrand', 'MainController@getTiresByBrand');
Route::post('/getDisksByBrand', 'MainController@getDisksByBrand');

Route::get('/tires', 'TiresController@index');
Route::get('/tire/{id}', 'TiresController@open');
Route::get('/getFilterDataForTires', 'TiresController@getFilterData');

Route::get('/disks', 'DisksController@index');
Route::get('/disk/{id}', 'DisksController@open');
Route::get('/getFilterDataForDisks', 'DisksController@getFilterData');

Route::get('/about', 'MainController@about');

Route::get('/delivery', 'MainController@delivery');

Route::get('/basket', 'BasketController@index');
Route::post('/order', 'BasketController@order');
Route::get('/getBasketCount', 'BasketController@count');

// Admin
Route::get('/admin', 'AdminController@index');
Route::post('/admin/accept/{id}', 'AdminController@accept');
Route::post('/admin/refuse/{id}', 'AdminController@refuse');
Route::get('/admin/completed', 'AdminController@completed');
Route::get('/admin/canceled', 'AdminController@canceled');

Route::get('/admin/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/admin/login', 'Auth\LoginController@login')->name('login');
Route::post('admin/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/mail', function () {
    $order = \App\Order::findOrFail(1);
    return new \App\Mail\SendOrder($order);
});