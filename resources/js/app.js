/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

const VueCookies = require('vue-cookies');
const VueScrollTo = require('vue-scrollto');

import VueMask from 'v-mask';
import {loadProgressBar} from 'axios-progress-bar';

Vue.use(VueCookies);
Vue.use(VueScrollTo);
Vue.use(VueMask);

$(window).on('load', function () {
    const $preloader = $('#preloader');
    const $loader = $preloader.find('svg');
    setTimeout(function () {
        $loader.fadeOut();
        $preloader.delay(350).fadeOut('slow');
    }, 1000);
});

loadProgressBar();

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component('tire-catalog', require('./components/TireCatalog.vue'));
Vue.component('disk-catalog', require('./components/DiskCatalog.vue'));
Vue.component('tire-open', require('./components/TireOpen.vue'));
Vue.component('disk-open', require('./components/DiskOpen.vue'));
Vue.component('basket', require('./components/Basket.vue'));

const app = new Vue({
    el: '#app',
    data: {
        count: 0,
        showSelector: false,

        brandTire: '',
        height: '',
        width: '',
        diameterTire: '',
        season: '',
        ship: '',
        reinforced: '',

        brandDisk: '',
        diameterDisk: '',
        dia: '',
        pcd: '',
        from_et: '',
        to_et: '',
        toEtList: [],
        type: '',

        vendor: '',
        model: '',
        year: '',
        modification: '',

        tires: {},
        disks: {},
        vendors: [],
        models: [],
        years: [],
        modifications: [],
        tiresByBrand: {
            factory: [],
            replace: [],
            tuning: [],
            factoryFront: [],
            factoryBack: [],
            factorySame: [],
            replaceFront: [],
            replaceBack: [],
            replaceSame: [],
            tuningFront: [],
            tuningBack: [],
            tuningSame: [],
        },
        disksByBrand: {
            factory: [],
            replace: [],
            tuning: [],
            factoryFront: [],
            factoryBack: [],
            factorySame: [],
            replaceFront: [],
            replaceBack: [],
            replaceSame: [],
            tuningFront: [],
            tuningBack: [],
            tuningSame: [],
            nut: '',
            pcd: '',
            diameter: ''
        },

        blockedInputs: [true, true, true],
        showTiresList: false,
        showDisksList: false,
    },
    mounted() {
        this.getMainFormData();
        axios.get('/getBasketCount').then(res => {
            this.count = res.data
        });
    },
    computed: {
        toTires: function () {
            return `/tires?brand=${this.brandTire}&height=${this.height}&width=${this.width}&diameter=${this.diameterTire}&season=${this.season}&thorns=${this.ship}&run_flat=${this.reinforced}`;
        },
        toDisks: function () {
            return `/disks?brand=${this.brandDisk}&diameter=${this.diameterDisk}&dia=${this.dia}&from_et=${this.from_et}&to_et=${this.to_et}&pcd=${this.pcd.replace('*', 'x')}&type=${this.type}`;
        }
    },
    methods: {
        getMainFormData: function () {
            axios.get('/getMainFormData').then((res) => {
                this.vendors = res.data.auto;
                this.disks = res.data.disks;
                this.tires = res.data.tires;
            })
        },
        setToEtList: function () {
            let that = this;
            this.toEtList = [];
            this.disks.et.forEach(function (item) {
                if (item > that.from_et) that.toEtList.push(item);
            })
        },
        setModelParams() {
            this.showTiresList = false;
            this.showDisksList = false;
            this.blockedInputs[0] = false;
            this.blockedInputs[1] = true;
            this.blockedInputs[2] = true;
            this.model = '';
            this.year = '';
            this.modification = '';
            axios.post('/getModelCar', {vendor: this.vendor,}).then((res) => {
                this.models = res.data.model;
            });
        },
        setYearParams() {
            this.showTiresList = false;
            this.showDisksList = false;
            this.blockedInputs[0] = false;
            this.blockedInputs[1] = false;
            this.blockedInputs[2] = true;
            this.year = '';
            this.modification = '';
            axios.post('/getYearCar', {vendor: this.vendor, model: this.model}).then((res) => {
                this.years = res.data.year;
            });
        },
        setModificationParams() {
            this.showTiresList = false;
            this.showDisksList = false;
            this.blockedInputs = [false, false, false];
            this.modification = '';
            axios.post('/getModificationCar', {vendor: this.vendor, model: this.model, year: this.year}).then((res) => {
                this.modifications = res.data.modification;
            });
        },
        splitParams(params) {
            if (params) {
                params = params.split(' # ').join('&');
                params = params.split(' #').join('&');
                params = params.split('# ').join('&');
                params = params.split('#').join('&');
                params = params.split('|').join('&');
                return params.split('&');
            }
        },
        getTiresByBrand() {
            axios.post('/getTiresByBrand', {
                vendor: this.vendor,
                model: this.model,
                year: this.year,
                modification: this.modification
            }).then((res) => {
                this.tiresByBrand.factory = res.data.tires.zavod_shini;
                this.tiresByBrand.replace = res.data.tires.zamen_shini;
                this.tiresByBrand.tuning = res.data.tires.tuning_shini;
              //  this.tiresByBrand.factory = this.splitParams(this.tiresByBrand.factory);
             //   this.tiresByBrand.replace = this.splitParams(this.tiresByBrand.replace);
                //  this.tiresByBrand.tuning = this.splitParams(this.tiresByBrand.tuning);
                this.tiresByBrand.factoryFront = this.vehicleParamsFront(this.tiresByBrand.factory).front;
                this.tiresByBrand.factoryBack = this.vehicleParamsFront(this.tiresByBrand.factory).back;
                this.tiresByBrand.factorySame = this.vehicleParamsFront(this.tiresByBrand.factory).same;
                this.tiresByBrand.replaceFront = this.vehicleParamsFront(this.tiresByBrand.replace).front;
                this.tiresByBrand.replaceBack = this.vehicleParamsFront(this.tiresByBrand.replace).back;
                this.tiresByBrand.replaceSame = this.vehicleParamsFront(this.tiresByBrand.replace).same;
                this.tiresByBrand.tuningFront = this.vehicleParamsFront(this.tiresByBrand.tuning).front;
                this.tiresByBrand.tuningBack = this.vehicleParamsFront(this.tiresByBrand.tuning).back;
                this.tiresByBrand.tuningSame = this.vehicleParamsFront(this.tiresByBrand.tuning).same;
            });
            this.showTiresList = true;
        },
        getDisksByBrand() {
            axios.post('/getDisksByBrand', {
                vendor: this.vendor,
                model: this.model,
                year: this.year,
                modification: this.modification
            }).then((res) => {
                this.disksByBrand.factory = res.data.disks.zavod_diskov;
                this.disksByBrand.replace = res.data.disks.zamen_diskov;
                this.disksByBrand.tuning = res.data.disks.tuning_diski;
                this.disksByBrand.nut = res.data.disks.gaika;
                this.disksByBrand.pcd = res.data.disks.pcd;
                this.disksByBrand.diameter = res.data.disks.diametr;
               // this.disksByBrand.factory = this.splitParams(this.disksByBrand.factory);
             //   this.disksByBrand.replace = this.splitParams(this.disksByBrand.replace);
                //  this.disksByBrand.tuning = this.splitParams(this.disksByBrand.tuning);
                this.disksByBrand.factoryFront = this.vehicleParamsFront(this.disksByBrand.factory).front;
                this.disksByBrand.factoryBack = this.vehicleParamsFront(this.disksByBrand.factory).back;
                this.disksByBrand.factorySame = this.vehicleParamsFront(this.disksByBrand.factory).same;
                this.disksByBrand.replaceFront = this.vehicleParamsFront(this.disksByBrand.replace).front;
                this.disksByBrand.replaceBack = this.vehicleParamsFront(this.disksByBrand.replace).back;
                this.disksByBrand.replaceSame = this.vehicleParamsFront(this.disksByBrand.replace).same;
                this.disksByBrand.tuningFront = this.vehicleParamsFront(this.disksByBrand.tuning).front;
                this.disksByBrand.tuningBack = this.vehicleParamsFront(this.disksByBrand.tuning).back;
                this.disksByBrand.tuningSame = this.vehicleParamsFront(this.disksByBrand.tuning).same;
            });
            this.showDisksList = true;
        },
        setLinkForTireList(item) {
            let diameterTires = item.split(' R')[1].trim();
            let widthTires = item.split(' R')[0].split('/')[0].trim();
            let heightTires = item.split(' R')[0].split('/')[1].trim();
            return `/tires?width=${widthTires}&height=${heightTires}&diameter=${diameterTires}`;
        },
        setLinkForDiskList(item) {
            let etDisks = item.split(' ET')[1].trim();
            let widthDisks = item.split(' ET')[0].split(' x ')[0].trim();
            let diameterDisks = item.split(' ET')[0].split(' x ')[1].trim();
            return `/disks?et=${etDisks}&width=${widthDisks.replace(',', '.')}&diameter=${diameterDisks}&pcd=${this.disksByBrand.pcd.replace('*', 'x')}&dia=${this.disksByBrand.diameter.split(' ')[0].replace(',', '.')}`;
        },
        resetParams() {
            this.showTiresList = false;
            this.showDisksList = false;
            this.blockedInputs = [true, true, true];
            this.vendor = '';
            this.model = '';
            this.year = '';
            this.modification = '';
        },
        addToBasket(id, count) {
            this.count++;
            this.$cookies.set(id, count, "0");
        },
        editFromBasket(id, count) {
            this.$cookies.set(id, count, "0");
        },
        removeFromBasket(id) {
            this.count--;
            this.$cookies.remove(id).set('basketCount', this.count, "0");
        },
        vehicleParamsFront(params) {
            let front = [];
            let back = [];
            let same = [];
            if (params) {
                let splittedArr = params.split('|');
                if (params.indexOf('#') + 1) {
                    splittedArr.forEach((elem) => {
                        if (~elem.indexOf('#')) {
                            let splittedElem = elem.split('#');
                            front.push(splittedElem[0]);
                            back.push(splittedElem[1]);
                        } else {
                            front.push(elem);
                        }
                    });
                } else {
                    same = splittedArr;
                }
            }
            return {front: front, back: back, same: same};
        }
    }
});
