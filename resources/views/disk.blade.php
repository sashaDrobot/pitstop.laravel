@extends('layouts.app')

@section('title', 'Диск '. $disk->name )

@section('content')

    <disk-open data="{{ json_encode($disk) }}"></disk-open>

@endsection