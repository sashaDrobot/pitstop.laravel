<!DOCTYPE html>
<html lang="ru" xmlns:v-on="http://www.w3.org/1999/xhtml" xmlns:v-bind="http://www.w3.org/1999/xhtml" xmlns: xmlns:>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#000">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css"
          href="https://cdn.rawgit.com/rikmms/progress-bar-4-axios/0a3acf92/dist/nprogress.css"/>
    <!-- Global site tag (gtag.js) - Google Ads: 801630399 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-801630399"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'AW-801630399');
    </script>
</head>
<body>
<div id="preloader" class="justify-content-center align-items-center fixed-top">
    <img src="{{ asset('img/preloader.svg') }}" alt="loader">
</div>
<div id="app">
    <nav class="navbar navbar-expand-lg fixed-top">
        <div class="container">
            <a class="navbar-brand" href="/"><img src="{{ asset('img/Logo.png') }}" alt="logo"></a>
            <div class="shopping-cart d-lg-none" style="z-index: 1050;">
                <a href="/basket">
                    <div class="shopping-cart-wrapper">
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                        <div class="shopping-cart-counter d-flex align-items-center justify-content-center text-center">
                            <span class="counter">@{{count}}</span>
                        </div>
                    </div>
                </a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item d-flex align-items-center justify-content-center text-center">
                        <a class="nav-link" href="/tires">Шины</a>
                    </li>
                    <li class="nav-item d-flex align-items-center justify-content-center text-center">
                        <a class="nav-link" href="/disks">Диски</a>
                    </li>
                    <li class="nav-item d-flex align-items-center justify-content-center text-center">
                        <a class="nav-link" href="/about">О нас</a>
                    </li>
                    <li class="nav-item d-flex align-items-center justify-content-center text-center">
                        <a class="nav-link" href="/delivery">Доставка/Оплата</a>
                    </li>
                    <li class="nav-item nav-contacts d-flex align-items-center justify-content-center">
                        <div class="row align-items-center justify-content-center d-none d-lg-flex">
                            <div class="col-3 nav-call-i d-flex align-items-center justify-content-center">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                            </div>
                            <div class="col-9 nav-contacts-info">
                                <ul>
                               {{--     <li>093 601 41 45</li>--}}
                                    <li>063 773 77 75</li>
                                    <li>096 773 77 75</li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item mr-0 d-flex align-items-center justify-content-center text-center">
                        <div class="row">
                            <div class="col-12 shopping-cart">
                                <a href="/basket">
                                    <div class="shopping-cart-wrapper d-none d-lg-block">
                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                        <div class="shopping-cart-counter d-flex align-items-center justify-content-center text-center">
                                            <span class="counter">@{{count}}</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

</div>
<footer class="container-fluid py-3">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-md-4">
                <div class="row align-items-center justify-content-center py-1">
                    <div class="col-md-2 d-flex d-md-block align-items-center justify-content-center text-center py-1">
                        <i class="fa fa-map-marker" aria-hidden="true"></i></div>
                    <div class="col-md-10 text-left">
                        <span>Работаем по всей Украине</span>
                    </div>
                </div>
                <div class="row align-items-center justify-content-center py-1">
                    <div class="col-md-2 d-flex d-md-block align-items-center justify-content-center text-center py-1">
                        <i class="fa fa-envelope" aria-hidden="true"></i></div>
                    <div class="col-md-10 text-left">
                        <a href="mailto:PitStopMDA97779@gmail.com">Email: PitStopMDA97779@gmail.com</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4  d-none d-md-flex align-items-center justify-content-center text-center qoob-studio">
                <a href="http://qoob-studio.com"><img src="{{ asset('img/logo_violet.png') }}" alt="qoob-studio"
                                                      class="qoob-studio-logo"></a>
            </div>
            <div class="col-md-4  d-flex justify-content-center flex-column footer-contacts-info">
                <div class="row align-items-center py-1">
                    <div class="col-md-2 d-flex d-md-block align-items-center justify-content-center offset-md-4 offset-lg-5 offset-xl-6 py-1">
                        <i class="fa fa-phone" aria-hidden="true" style="transform: rotate(-90deg)"></i>
                    </div>
                    <div class="col-md-6 col-lg-5 col-xl-4 pl-0 text-center">
                        <ul class="mb-0 pl-0">
                          {{--  <li>093 601 41 45</li>--}}
                            <li>063 773 77 75</li>
                            <li>096 773 77 75</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12  text-center qoob-studio">
                <a href="http://qoob-studio.com" class="d-block d-md-none mb-2"><img
                            src="{{ asset('img/logo_violet.png') }}" alt="qoob-studio" class="qoob-studio-logo"></a>
                <h6>Copyright © 2018 PITSTOP</h6>
            </div>

        </div>
    </div>
</footer>
<script src="{{ asset('js/wow.js') }}"></script>
<script>
    new WOW().init();
</script>
</body>
</html>
