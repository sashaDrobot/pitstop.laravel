@extends('layouts.app')

@section('title', 'Шина ' . $tire->name )

@section('content')

    <tire-open data="{{ json_encode($tire) }}"></tire-open>

@endsection