@extends('layouts.app')

@section('title', 'Каталог шин')

@section('content')

    <tire-catalog data="{{ json_encode($props) }}"></tire-catalog>

@endsection