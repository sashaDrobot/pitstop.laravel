<!DOCTYPE html>
<html lang="ru" xmlns:v-on="http://www.w3.org/1999/xhtml" xmlns:v-bind="http://www.w3.org/1999/xhtml" xmlns: xmlns:>
<head>
    <meta charset="UTF-8">
    <title>Главная</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#000">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
</head>
<body>
<div>
    <nav class="navbar d-block d-lg-none navbar-expand-lg fixed-top">
        <div class="container">
            <a class="navbar-brand" href="/"><img src="{{ asset('img/Logo.png') }}" alt="logo"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item d-flex align-items-center justify-content-center text-center">
                        <a class="nav-link" href="/admin">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i> Новые заказы
                        </a>
                    </li>
                    <li class="nav-item d-flex align-items-center justify-content-center text-center">
                        <a class="nav-link" href="/admin/completed">
                            <i class="fa fa-check" aria-hidden="true"></i> Обработанные
                        </a>
                    </li>
                    <li class="nav-item d-flex align-items-center justify-content-center text-center">
                        <a class="nav-link" href="/admin/canceled">
                            <i class="fa fa-times" aria-hidden="true"></i> Отмененные
                        </a>
                    </li>
                    <li class="nav-item d-flex align-items-center justify-content-center text-center">
                        <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out" aria-hidden="true"></i> Выход
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container-fluid admin-container">
        <nav class="navbar d-none d-lg-block navbar-expand-lg fixed-top">
            <div class="container">
                <a class="navbar-brand" href="/"><img src="{{ asset('img/Logo.png') }}" alt="logo"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item d-flex align-items-center justify-content-center text-center">
                            <i class="fa fa-user-circle" aria-hidden="true"></i><span class="username ml-1">Админ</span>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="row admin-inner">
            <div class="col-md-2 d-none d-lg-block admin-menu">
                <div class="{{ Request::is('admin') ? 'admin-menu-item-active' : '' }} row admin-menu-item align-items-center">
                    <div class="col-12">
                        <a href="/admin">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i> Новые заказы
                        </a>
                    </div>
                </div>
                <div class="{{ Request::is('admin/completed') ? 'admin-menu-item-active' : '' }} row admin-menu-item align-items-center">
                    <div class="col-12">
                        <a href="/admin/completed">
                            <i class="fa fa-check" aria-hidden="true"></i> Обработанные
                        </a>
                    </div>
                </div>
                <div class="{{ Request::is('admin/canceled') ? 'admin-menu-item-active' : '' }} row admin-menu-item align-items-center">
                    <div class="col-12">
                        <a href="/admin/canceled">
                            <i class="fa fa-times" aria-hidden="true"></i> Отмененные
                        </a>
                    </div>
                </div>
                <div class="row admin-menu-item align-items-center">
                    <div class="col-12">
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out" aria-hidden="true"></i> Выход
                        </a>
                        <form id="logout-form" class="d-none" action="{{ route('logout') }}" method="POST">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>

            @yield('content')

        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
</body>
</html>