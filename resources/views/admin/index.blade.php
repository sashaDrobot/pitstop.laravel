@extends('admin.layout')

@section('content')
    <div class="col-md-10 offset-md-2 orders-wrapper">
        <div class="row">
            <div class="col-12 text-left">
                <h1>Заказы</h1>
            </div>
        </div>
        <div class="row orders">
            @foreach($orders as $order)
                <div class="col-md-6 order-wrapper">
                    <div class="row p-3">
                        <div class="col-12 order-inner">
                            <div class="row py-1">
                                <div class="col-1 text-center">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                </div>
                                <div class="col-10 order-info">
                                    <span>Ф.И.О : </span> {{ $order->name }}
                                </div>
                            </div>
                            <div class="row py-1">
                                <div class="col-1 text-center">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                </div>
                                <div class="col-10 order-info">
                                    <span>E-mail: </span> {{ $order->email }}
                                </div>
                            </div>
                            <div class="row py-1">
                                <div class="col-1 text-center">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                </div>
                                <div class="col-10 order-info">
                                    <span>Номер телефона: </span> {{ $order->phone }}
                                </div>
                            </div>
                            <div class="row py-1">
                                <div class="col-1 text-center">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                </div>
                                <div class="col-10 order-info">
                                    <span>Город: </span> {{ $order->city }}
                                </div>
                            </div>
                            <div class="row py-1">
                                <div class="col-1 text-center">
                                    <i class="fa fa-truck" aria-hidden="true"></i>
                                </div>
                                <div class="col-10 order-info">
                                    <span>Способ доставки: </span> {{ $order->delivery }}
                                </div>
                            </div>
                            <div class="row py-1">
                                <div class="col-1 text-center">
                                    <i class="fa fa-building" aria-hidden="true"></i>
                                </div>
                                <div class="col-10 order-info">
                                    <span>Адрес: </span> {{ $order->address }}
                                </div>
                            </div>
                            <div class="row py-1">
                                <div class="col-1 text-center">
                                    <i class="fa fa-building" aria-hidden="true"></i>
                                </div>
                                <div class="col-10 order-info">
                                    <span>Способ оплаты: </span> {{ $order->payment }}
                                </div>
                            </div>
                            <div class="row py-1">
                                <div class="col-1 text-center">
                                    <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                                </div>
                                <div class="col-10 order-info">
                                    <div class="row">
                                        <div class="col-3">
                                            <span>Товары: </span>
                                        </div>
                                        <div class="col-9 order-goods">
                                            @foreach($order->products as $product)
                                                <div class="row">
                                                    <div class="col-12">{{ $product['name'] }}
                                                        <span class="order-goods-counter"> * {{ $product['count'] }}</span>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row py-1 order-btns">
                                <div class="col-lg-6 text-center">
                                    <form action="/admin/refuse/{{ $order->id }}" method="post">
                                        @csrf
                                        <button type="submit" class="mda-btn cancel-btn my-1">Отказать</button>
                                    </form>
                                </div>
                                <div class="col-lg-6 text-center">
                                    <form action="/admin/accept/{{ $order->id }}" method="post">
                                        @csrf
                                        <button type="submit" class="mda-btn my-1">Принять</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection