@extends('layouts.app')

@section('title', 'Каталог дисков')

@section('content')

    <disk-catalog data="{{ json_encode($props) }}"></disk-catalog>

@endsection