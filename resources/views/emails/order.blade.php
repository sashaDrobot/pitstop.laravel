@component('mail::message')
    # Был оформлен новый заказ!

    Контактные данные:

    Имя: {{ $order->name }}
    Телефон: {{ $order->phone }}
    Email: {{ $order->email }}
    Город: {{ $order->city }}
    Способ доставки: {{ $order->delivery }}
    Адрес: {{ $order->address }}
    Способ оплаты: {{ $order->payment }}
    Товары:
    @foreach($order->products as $product)
        {{ $product['name'] }} * {{ $product['count'] }}
    @endforeach

@endcomponent