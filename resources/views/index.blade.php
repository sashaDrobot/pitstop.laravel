@extends('layouts.app')

@section('title', 'Pitstop - Главная')

@section('content')
    <div class="container-fluid main-container" xmlns:v-on="http://www.w3.org/1999/xhtml">
        <div class="container tier-main-container">
            <transition name="fade">
                <div class="row align-items-center justify-content-center" v-show="!showSelector">
                    <div class="col-lg-4 d-flex align-items-start justify-content-center flex-column main-headers">
                        <h1>Шины</h1>
                        <h2>И диски</h2>
                        <span class="h-line"></span>
                        <h6>Лучшее предложение для вашего автомобиля</h6>
                    </div>
                    <div class="col-8 col-lg-4 wheel-wrapper">
                        <div class="row d-flex align-items-center justify-content-center  ">
                            <img src="{{ asset('img/tier.png') }}" id="tier">
                            <img src="{{ asset('img/disc.png') }}" id="disk">
                        </div>
                        <div class="row align-items-center justify-content-center">
                            <div class="col-12 text-center">
                                <button @click="showSelector=!showSelector" class="mda-btn">
                                    Подобрать
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 main-advantages text-right">
                        <ul class="advantages">
                            <li class="d-flex align-items-center justify-content-start"><span
                                        class="o-marker"></span><span
                                        class="adv-inner">Широкий ассортимент</span></li>
                            <li class="d-flex align-items-center justify-content-start"><span
                                        class="o-marker"></span><span
                                        class="adv-inner">Гарантия качества</span></li>
                            <li class="d-flex align-items-center justify-content-start"><span
                                        class="o-marker"></span><span
                                        class="adv-inner">Подбор лучшей модели</span></li>
                            <li class="d-flex align-items-center justify-content-start"><span
                                        class="o-marker"></span><span
                                        class="adv-inner">Быстрая доставка</span></li>
                        </ul>
                    </div>
                </div>
            </transition>
            <transition name="fade">
                <div class="row align-items-center justify-content-center flex-column main-selector-row"
                     v-show="showSelector">
                    <div class="col-lg-8" id="main-selector">
                        <div class="row selector-main flex-column align-items-center justify-content-center">
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-tiers-tab" data-toggle="pill"
                                       href="#pills-tiers" role="tab" aria-controls="pills-tiers" aria-selected="true" @click="resetParams">Шины</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-disks-tab" data-toggle="pill" href="#pills-disks"
                                       role="tab" aria-controls="pills-disks" aria-selected="false" @click="resetParams">Диски</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-tiers" role="tabpanel"
                                     aria-labelledby="pills-tiers-tab">
                                    <ul class="nav nav-pills mb-3" id="pills-tab-tiers-1" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="pills-tiers-inner-tab-1" data-toggle="pill"
                                               href="#pills-tiers-inner-1" role="tab"
                                               aria-controls="pills-tiers-inner-1" aria-selected="true">По размеру</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="pills-tiers-inner-tab-2" data-toggle="pill"
                                               href="#pills-tiers-inner-2" role="tab"
                                               aria-controls="pills-tiers-inner-2" aria-selected="false">По авто</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="pills-tabContent-tiers-1">
                                        <div class="tab-pane fade show active" id="pills-tiers-inner-1" role="tabpanel"
                                             aria-labelledby="pills-tiers-tab-1">
                                            <form>
                                                <div class="row form-elements">
                                                    <div class="col-lg-3 form-elem">
                                                        <label for="brand" class="mt-2">
                                                            <img src="{{ asset('img/vendor.png') }}"
                                                                 class="selector-icon">
                                                            <span>Бренд</span>
                                                        </label>
                                                        <select v-model="brandTire" id="brand">
                                                            <option value="">Все</option>
                                                            <option v-for="brand in tires.brands" :value="brand">
                                                                @{{brand}}
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-3 form-elem">
                                                        <label for="width" class="mt-2">
                                                            <img src="{{ asset('img/width.png') }}"
                                                                 class="selector-icon">
                                                            Ширина
                                                        </label>
                                                        <select v-model="width" id="width">
                                                            <option value="">Все</option>
                                                            <option v-for="width in tires.width" :value="width">
                                                                @{{width}}
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-3 form-elem">
                                                        <label for="height" class="mt-2">
                                                            <img src="{{ asset('img/height.png') }}"
                                                                 class="selector-icon">
                                                            Профиль
                                                        </label>
                                                        <select v-model="height" id="height">
                                                            <option value="">Все</option>
                                                            <option v-for="height in tires.height" :value="height">
                                                                @{{height}}
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-3 form-elem">
                                                        <label for="diameter" class="mt-2">
                                                            <img src="{{ asset('img/diameter.png') }}"
                                                                 class="selector-icon">
                                                            Диаметр
                                                        </label>
                                                        <select v-model="diameterTire" id="diameter">
                                                            <option value="">Все</option>
                                                            <option v-for="diameter in tires.diameters"
                                                                    :value="diameter">@{{diameter}}
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row form-elements">
                                                    <div class="col-lg-3 form-elem">
                                                        <label for="season" class="mt-2">Сезон</label>
                                                        <select v-model="season" id="season">
                                                            <option value="">Все</option>
                                                            <option value="летняя">Лето</option>
                                                            <option value="зимняя">Зима</option>
                                                            <option value="всесезонная">Всесезон</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-3 form-elem">
                                                        <label for="ship" class="mt-2">Шипы</label>
                                                        <select v-model="ship" id="ship">
                                                            <option value="">Неважно</option>
                                                            <option value="1">Да</option>
                                                            <option value="0">Нет</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-3 form-elem">
                                                        <label for="reinforced" class="mt-2">Run flat</label>
                                                        <select v-model="reinforced" id="reinforced">
                                                            <option value="">Неважно</option>
                                                            <option value="1">Да</option>
                                                            <option value="0">Нет</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 text-center">
                                                        <a :href="toTires" class="mda-btn">
                                                            Подобрать
                                                        </a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade" id="pills-tiers-inner-2" role="tabpanel"
                                             aria-labelledby="pills-tiers-tab-2">
                                            <form>
                                                <div class="row form-elements">
                                                    <div class="col-lg-3 form-elem">
                                                        <label for="vendor" class="mt-2">
                                                            <img src="{{ asset('img/zvezda-model.png') }}"
                                                                 class="selector-icon">
                                                            <span>Марка</span>
                                                        </label>
                                                        <select v-model="vendor" id="vendor" @change="setModelParams">
                                                            <option disabled value="">Выберите</option>
                                                            <option v-for="vendor in vendors" :value="vendor">
                                                                @{{vendor}}
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-3 form-elem">
                                                        <label for="model" class="mt-2">
                                                            <img src="{{ asset('img/zvezdavzvezde.png') }}"
                                                                 class="selector-icon">
                                                            <span>Модель</span>
                                                        </label>
                                                        <select v-model="model" id="model" :disabled="blockedInputs[0]"
                                                                @change="setYearParams">
                                                            <option disabled value="">Выберите</option>
                                                            <option v-for="model in models" :value="model">@{{model}}
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-3 form-elem">
                                                        <label for="year" class="mt-2">
                                                            <img src="{{ asset('img/graduationyear.png') }}"
                                                                 class="selector-icon">
                                                            <span>Год</span>
                                                        </label>
                                                        <select v-model="year" id="year" :disabled="blockedInputs[1]" @change="setModificationParams">
                                                            <option disabled value="">Выберите</option>
                                                            <option v-for="year in years" :value="year">@{{year}}</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-3 form-elem">
                                                        <label for="variant" class="mt-2">
                                                            <img src="{{ asset('img/itemsutility.png') }}"
                                                                 class="selector-icon">
                                                            <span>Модификация</span>
                                                        </label>
                                                        <select v-model="modification" id="variant" :disabled="blockedInputs[2]" @change="getTiresByBrand">
                                                            <option disabled value="">Выберите</option>
                                                            <option v-for="modification in modifications" :value="modification">@{{modification}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <transition name="fade">
                                                    <div class="row auto-res-row py-4" v-if="showTiresList">
                                                        <div v-if="tiresByBrand.factory != null" class="col-lg-12">
                                                            <span class="auto-res-headers">Заводская комплектация:</span>
                                                            <ul class="auto-variants-list">
                                                                <li v-for="item in tiresByBrand.factorySame"><a target="_blank" :href="setLinkForTireList(item)">@{{ item }}</a></li>
                                                            </ul>
                                                            <div v-if="tiresByBrand.factorySame.length === 0" class="d-flex justify-content-around align-content-around">
                                                                <div>
                                                                    <p class="auto-res-headers mb-0">передняя ось:</p>
                                                                    <ul class="auto-variants-list">
                                                                        <li v-for="item in tiresByBrand.factoryFront"><a target="_blank" :href="setLinkForTireList(item)">@{{ item }}</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div>
                                                                    <p class="auto-res-headers mb-0">задняя ось:</p>
                                                                    <ul class="auto-variants-list">
                                                                        <li v-for="item in tiresByBrand.factoryBack"><a target="_blank" :href="setLinkForTireList(item)">@{{ item }}</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div v-if="tiresByBrand.replace != null" class="col-lg-12">
                                                            <span class="auto-res-headers">Варианты замены:</span>
                                                            <ul class="auto-variants-list">
                                                                <li v-for="item in tiresByBrand.replaceSame"><a target="_blank" :href="setLinkForTireList(item)">@{{ item }}</a></li>
                                                            </ul>
                                                            <div v-if="tiresByBrand.replaceSame.length === 0" class="d-flex justify-content-around align-content-around">
                                                                <div>
                                                                    <p class="auto-res-headers mb-0">передняя ось:</p>
                                                                    <ul class="auto-variants-list">
                                                                        <li v-for="item in tiresByBrand.replaceFront"><a target="_blank" :href="setLinkForTireList(item)">@{{ item }}</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div>
                                                                    <p class="auto-res-headers mb-0">задняя ось:</p>
                                                                    <ul class="auto-variants-list">
                                                                        <li v-for="item in tiresByBrand.replaceBack"><a target="_blank" :href="setLinkForTireList(item)">@{{ item }}</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div v-if="tiresByBrand.tuning != null" class="col-lg-12">
                                                            <span class="auto-res-headers">Тюнинг:</span>
                                                            <ul class="auto-variants-list">
                                                                <li v-for="item in tiresByBrand.tuningSame"><a target="_blank" :href="setLinkForTireList(item)">@{{ item }}</a></li>
                                                            </ul>
                                                            <div v-if="tiresByBrand.tuningSame.length == 0" class="d-flex justify-content-around align-content-around">
                                                                <div>
                                                                    <p class="auto-res-headers mb-0">передняя ось:</p>
                                                                    <ul class="auto-variants-list">
                                                                        <li v-for="item in tiresByBrand.tuningFront"><a target="_blank" :href="setLinkForTireList(item)">@{{ item }}</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div>
                                                                    <p class="auto-res-headers mb-0">задняя ось:</p>
                                                                    <ul class="auto-variants-list">
                                                                        <li v-for="item in tiresByBrand.tuningBack"><a target="_blank" :href="setLinkForTireList(item)">@{{ item }}</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </transition>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pills-disks" role="tabpanel"
                                     aria-labelledby="pills-disks-tab">
                                    <ul class="nav nav-pills mb-3" id="pills-tab-disks-1" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="pills-disks-inner-tab-1" data-toggle="pill"
                                               href="#pills-disks-inner-1" role="tab"
                                               aria-controls="pills-disks-inner-1" aria-selected="true">По размеру</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="pills-disks-inner-tab-2" data-toggle="pill"
                                               href="#pills-disks-inner-2" role="tab"
                                               aria-controls="pills-disks-inner-2" aria-selected="false">По авто</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="pills-tabContent-disks-1">
                                        <div class="tab-pane fade show active" id="pills-disks-inner-1" role="tabpanel"
                                             aria-labelledby="pills-disks-tab-1">
                                            <form>
                                                <div class="row form-elements">
                                                    <div class="col-lg-3 form-elem">
                                                        <label for="brandDisk" class="mt-2">
                                                            <img src="{{ asset('img/vendor.png') }}"
                                                                 class="selector-icon">
                                                            <span>Бренд</span>
                                                        </label>
                                                        <select v-model="brandDisk" id="brandDisk">
                                                            <option value="">Все</option>
                                                            <option v-for="brand in disks.brands" :value="brand">
                                                                @{{brand}}
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-3 form-elem">
                                                        <label for="dia" class="mt-2">
                                                            <img src="{{ asset('img/height.png') }}"
                                                                 class="selector-icon">
                                                            DIA
                                                        </label>
                                                        <select v-model="dia" id="dia">
                                                            <option value="">Все</option>
                                                            <option v-for="dia in disks.dia" :value="dia">@{{dia}}
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-3 form-elem">
                                                        <label for="pcd" class="mt-2">
                                                            <img src="{{ asset('img/PCD.png') }}" class="selector-icon">
                                                            PCD
                                                        </label>
                                                        <select v-model="pcd" id="pcd">
                                                            <option value="">Все</option>
                                                            <option v-for="pcd in disks.pcd" :value="pcd">@{{pcd}}
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-3 form-elem">
                                                        <label for="diameterDisk" class="mt-2">
                                                            <img src="{{ asset('img/radius-disc.png') }}"
                                                                 class="selector-icon">
                                                            Диаметр
                                                        </label>
                                                        <select v-model="diameterDisk" id="diameterDisk">
                                                            <option value="">Все</option>
                                                            <option v-for="diameter in disks.diameters"
                                                                    :value="diameter">@{{diameter}}
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row form-elements">
                                                    <div class="col-lg-3 form-elem">
                                                        <label for="from_et" class="mt-2">
                                                            ET от
                                                        </label>
                                                        <select v-model="from_et" @change="setToEtList()" id="from_et">
                                                            <option value="">Неважно</option>
                                                            <option v-for="et in disks.et" :value="et">@{{et}}</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-3 form-elem">
                                                        <label for="to_et" class="mt-2">
                                                            ET до
                                                        </label>
                                                        <select v-model="to_et" id="to_et">
                                                            <option value="">Неважно</option>
                                                            <option v-for="et in toEtList" :value="et">@{{et}}</option>
                                                        </select>
                                                    </div>
                                                   {{-- <div class="col-lg-3 form-elem">
                                                        <label for="type" class="mt-2">
                                                            Тип
                                                        </label>
                                                        <select v-model="type" id="type">
                                                            <option value="">Все</option>
                                                            <option value="стальной">Стальные</option>
                                                            <option value="литой">Литые</option>
                                                            <option value="кованый">Кованые</option>
                                                        </select>
                                                    </div>--}}
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 text-center">
                                                        <a :href="toDisks" class="mda-btn">
                                                            Подобрать
                                                        </a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade" id="pills-disks-inner-2" role="tabpanel"
                                             aria-labelledby="pills-disks-tab-2">
                                            <form>
                                                <div class="row form-elements">
                                                    <div class="col-lg-3 form-elem">
                                                        <label for="vendor" class="mt-2">
                                                            <img src="{{ asset('img/zvezda-model.png') }}"
                                                                 class="selector-icon">
                                                            <span>Марка</span>
                                                        </label>
                                                        <select v-model="vendor" id="vendor" @change="setModelParams">
                                                            <option disabled value="">Выберите</option>
                                                            <option v-for="vendor in vendors" :value="vendor">
                                                                @{{vendor}}
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-3 form-elem">
                                                        <label for="model" class="mt-2">
                                                            <img src="{{ asset('img/zvezdavzvezde.png') }}"
                                                                 class="selector-icon">
                                                            <span>Модель</span>
                                                        </label>
                                                        <select v-model="model" id="model" :disabled="blockedInputs[0]"
                                                                @change="setYearParams">
                                                            <option disabled value="">Выберите</option>
                                                            <option v-for="model in models" :value="model">@{{model}}
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-3 form-elem">
                                                        <label for="year" class="mt-2">
                                                            <img src="{{ asset('img/graduationyear.png') }}"
                                                                 class="selector-icon">
                                                            <span>Год</span>
                                                        </label>
                                                        <select v-model="year" id="year" :disabled="blockedInputs[1]" @change="setModificationParams">
                                                            <option disabled value="">Выберите</option>
                                                            <option v-for="year in years" :value="year">@{{year}}
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-3 form-elem">
                                                        <label for="variant" class="mt-2">
                                                            <img src="{{ asset('img/itemsutility.png') }}"
                                                                 class="selector-icon">
                                                            <span>Модификация</span>
                                                        </label>
                                                        <select v-model="modification" id="variant" :disabled="blockedInputs[2]" @change="getDisksByBrand">
                                                            <option disabled value="">Выберите</option>
                                                            <option v-for="modification in modifications" :value="modification">@{{modification}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <transition name="fade">
                                                    <div class="row auto-res-row py-4" v-if="showDisksList">
                                                        <div class="col-lg-6 offset-lg-3">
                                                            <span class="auto-res-headers">Общие параметры:</span>
                                                            <ul class="auto-variants-list">
                                                                <li>@{{ disksByBrand.nut }}</li>
                                                                <li>@{{ disksByBrand.pcd }}</li>
                                                                <li>@{{ disksByBrand.diameter }}</li>
                                                            </ul>
                                                        </div>
                                                        <div v-if="disksByBrand.factory != null" class="col-lg-12">
                                                            <span class="auto-res-headers">Заводская комплектация:</span>
                                                            <ul class="auto-variants-list">
                                                                <li v-for="item in disksByBrand.factorySame"><a target="_blank" :href="setLinkForDiskList(item)">@{{ item }}</a></li>
                                                            </ul>
                                                            <div v-if="disksByBrand.factorySame.length === 0" class="d-flex justify-content-around align-content-around">
                                                                <div>
                                                                    <p class="auto-res-headers mb-0">передняя ось:</p>
                                                                    <ul class="auto-variants-list">
                                                                        <li v-for="item in disksByBrand.factoryFront"><a target="_blank" :href="setLinkForDiskList(item)">@{{ item }}</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div>
                                                                    <p class="auto-res-headers mb-0">задняя ось:</p>
                                                                    <ul class="auto-variants-list">
                                                                        <li v-for="item in disksByBrand.factoryBack"><a target="_blank" :href="setLinkForDiskList(item)">@{{ item }}</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div v-if="disksByBrand.replace != null" class="col-lg-12">
                                                            <span class="auto-res-headers">Варианты замены:</span>
                                                            <ul class="auto-variants-list">
                                                                <li v-for="item in disksByBrand.replaceSame"><a target="_blank" :href="setLinkForDiskList(item)">@{{ item }}</a></li>
                                                            </ul>
                                                            <div v-if="disksByBrand.replaceSame.length === 0" class="d-flex justify-content-around align-content-around">
                                                                <div>
                                                                    <p class="auto-res-headers mb-0">передняя ось:</p>
                                                                    <ul class="auto-variants-list">
                                                                        <li v-for="item in disksByBrand.replaceFront"><a target="_blank" :href="setLinkForDiskList(item)">@{{ item }}</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div>
                                                                    <p class="auto-res-headers mb-0">задняя ось:</p>
                                                                    <ul class="auto-variants-list">
                                                                        <li v-for="item in disksByBrand.replaceBack"><a target="_blank" :href="setLinkForDiskList(item)">@{{ item }}</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div v-if="disksByBrand.tuning != null" class="col-lg-12">
                                                            <span class="auto-res-headers">Тюнинг:</span>
                                                            <ul class="auto-variants-list">
                                                                <li v-for="item in disksByBrand.tuningSame"><a target="_blank" :href="setLinkForDiskList(item)">@{{ item }}</a></li>
                                                            </ul>
                                                            <div v-if="disksByBrand.tuningSame.length == 0" class="d-flex justify-content-around align-content-around">
                                                                <div>
                                                                    <p class="auto-res-headers mb-0">передняя ось:</p>
                                                                    <ul class="auto-variants-list">
                                                                        <li v-for="item in disksByBrand.tuningFront"><a target="_blank" :href="setLinkForDiskList(item)">@{{ item }}</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div>
                                                                    <p class="auto-res-headers mb-0">задняя ось:</p>
                                                                    <ul class="auto-variants-list">
                                                                        <li v-for="item in disksByBrand.tuningBack"><a target="_blank" :href="setLinkForDiskList(item)">@{{ item }}</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </transition>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="close-selector" v-on:click="showSelector=!showSelector">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </transition>
        </div>
    </div>
@endsection