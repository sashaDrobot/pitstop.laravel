@extends('layouts.app')

@section('title', 'Корзина')

@section('content')

    <basket data="{{ json_encode($data) }}"></basket>

@endsection