@extends('layouts.app')

@section('title', 'Оплата и Доставка')

@section('content')
    <div class="container-fluid delivery-container-wrapper d-flex">
        <div class="container delivery-container">
            <div class="row py-5">
                <div class="col-12 text-center delivery-header">
                    <h2>ДОСТАВКА ТОВАРА</h2>
                    <hr>
                </div>
                <div class="col-12 text-left delivery-info">
                    <p>Доставка по Украине осуществляется курьерскими службами: «Новая Почта», «Ин-Тайм» на ваш
                        выбор. Выяснить расположение пунктов приема-выдачи этих перевозчиков вы можете на их официальных
                        сайтах.</p>
                </div>
                <div class="col-6 wow jackInTheBox text-center delivery-img-wrapper">
                    <img src="{{ asset('img/nova-pochta.png') }}" alt="Новая Почта" class="img-fluid">
                </div>
                <div class="col-6 wow jackInTheBox text-center delivery-img-wrapper">
                    <img src="{{ asset('img/int-time.png') }}" alt="Ин Тайм" class="img-fluid">
                </div>
                <div class="col-12 text-center delivery-sub-info py-2">
                    <p>Сроки доставки: согласно графику доставки «Нова пошта» и «ІнТайм»</p>
                </div>
            </div>
            <div class="row py-5">
                <div class="col-12 text-center delivery-header">
                    <h2>СПОСОБЫ ОПЛАТЫ ТОВАРА</h2>
                    <hr>
                </div>
                <div class="col-12 py-3">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-10 text-left delivery-info">
                            <h6>Наложенный платеж</h6>
                            <p>Оплата наложенным платежом осуществляется при получении товара через курьерскую службу (в
                                отделении курьерской службы или у курьера). Данная услуга является платной и её
                                стоимость рассчитывается согласно тарифов курьерской службы.
                            </p>
                        </div>
                        <div class="col-md-2 wow jackInTheBox text-center delivery-img-wrapper">
                            <img src="{{ asset('img/perevod.png') }}" alt="Наложенный платеж" class="img-fluid">
                        </div>
                    </div>
                </div>
                <div class="col-12 py-3">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-10 text-left delivery-info">
                            <h6>На карту Visa, MasterCard</h6>
                            <p>После оформления заказа выберите способ оплаты "Кредитная карта" - система перенаправит
                                Вас на страницу интернет-эквайринга банка. Как только Вы введете данные карты - оплата
                                за выбранную услугу будет зачтена незамедлительно.
                            </p>
                        </div>
                        <div class="col-md-2 wow jackInTheBox text-center delivery-img-wrapper">
                            <img src="{{ asset('img/visa.png') }}" alt="visa" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection