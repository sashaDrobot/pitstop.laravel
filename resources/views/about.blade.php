@extends('layouts.app')

@section('title', 'О нас')

@section('content')
    <div class="container-fluid delivery-container-wrapper d-flex">
        <div class="container delivery-container py-5">
            <div class="row py-2 wow fadeIn">
                <div class="col-12 text-center delivery-header">
                    <h2>О НАС</h2>
                    <hr>
                </div>
                <div class="col-12 text-center delivery-info">
                    <p>Компания «PitstopMDA» является интернет-магазином дисков и шин и успешно ведёт торговлю более
                        десяти лет на рынке Украины.
                    </p>
                    <p>Мы сотрудничаем с известными логистическими компаниями, поэтому осуществляем быструю доставку
                        продукции в более чем 1500 населенных пунктов Украины. Такого результата мы добились благодаря
                        широкому ассортименту изделий, высококачественному обслуживанию и демократичной ценовой
                        политике.</p>
                    <p>Наша компания работает с огромным количеством проверенных поставщиков, поэтому гарантирует
                        качество и надежность предлагаемых товаров.
                    </p>
                </div>
            </div>
            <div class="row py-2">
                <div class="col-12 text-center delivery-header">
                    <h2>ПРЕИМУЩЕСТВА</h2>
                    <hr>
                </div>
                <div class="col-12 py-3">
                    <div class="row advantages-row justify-content-center">
                        <div class="col-md-3 d-flex align-items-center flex-column adv-elem-wrapper text-center wow bounceIn">
                            <img src="{{ asset('img/assort.png') }}" alt="range" class="img-fluid mb-3">
                            <h6>Широкий ассортимент</h6>
                            <p>Огромный выбор качественных шин и дисков.</p>
                        </div>
                        <div class="col-md-3 d-flex align-items-center flex-column adv-elem-wrapper text-center wow bounceIn">
                            <img src="{{ asset('img/quality-assurance.png') }}" alt="quality" class="img-fluid mb-3">
                            <h6>Гарантия качества</h6>
                            <p>Нашей главной целью и основополагающим принципом в работе является удовлетворенность
                                клиентов.</p>
                        </div>
                        <div class="col-md-3 d-flex align-items-center flex-column adv-elem-wrapper text-center wow bounceIn">
                            <img src="{{ asset('img/model-top.png') }}" alt="selection" class="img-fluid mb-3">
                            <h6>Подбор лучшей модели</h6>
                            <p>Мы стремимся предоставить максимум полезной информации о продаваемых товарах.</p>
                        </div>
                        <div class="col-md-3 d-flex align-items-center flex-column adv-elem-wrapper text-center wow bounceIn">
                            <img src="{{ asset('img/speed-delivery.png') }}" alt="delivery" class="img-fluid mb-3">
                            <h6>Быстрая доставка</h6>
                            <p>Доставка по Украине осуществляется курьерскими службами: «Новая Почта» и «Ин-Тайм» на Ваш
                                выбор.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection