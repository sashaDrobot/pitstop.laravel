<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tires extends Model
{
    protected $table = 'price_shini';
    public $timestamps = false;

    public function insertTires($value)
    {
        $this->product_id = $value->product_id;
        $this->brand = $value->brand;
        $this->height = str_replace(',', '.', $value->height);
        $this->end_opt_price = str_replace(',', '.', $value->end_opt_price);
        $this->end_rozn_price = str_replace(',', '.', $value->end_rozn_price);
        $this->year = $value->year;
        $this->supplier_city = $value->supplier_city;
        $this->date = $value->date;
        $this->diameter = str_replace(',', '.', $value->diameter);
        $this->load_index = $value->load_index;
        $this->speed_index = $value->speed_index;
        $this->model = $value->model;
        $this->name = $value->name;
        $this->ini_opt_price = $value->ini_opt_price;
        $this->amount = $value->amount;
        $this->supplier = $value->supplier;
        $this->ini_rozn_price = $value->ini_rozn_price;
        $this->season = $value->season;
        $this->country = $value->country;
        $this->wehicle_type = $value->wehicle_type;
        $this->reinforced = $value->reinforced;
        $this->photo = '/img/' . $value->photo;
        $this->ship = $value->ship;
        $this->width = str_replace(',', '.', $value->width);
        $this->save();
    }
}
