<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Selection extends Model
{
    protected $table = 'podbor_shini_i_diski';
    public $timestamps = false;

    public function insertSelection($value)
    {
        $this->id = $value->id;
        $this->vendor = $value->vendor;
        $this->car = $value->car;
        $this->year = $value->year;
        $this->modification = $value->modification;
        $this->pcd = $value->pcd;
        $this->diametr = $value->diametr;
        $this->gaika = $value->gaika;
        $this->zavod_shini = $value->zavod_shini;
        $this->zamen_shini = $value->zamen_shini;
        $this->tuning_shini = $value->tuning_shini;
        $this->zavod_diskov = $value->zavod_diskov;
        $this->zamen_diskov = $value->zamen_diskov;
        $this->tuning_diski = $value->tuning_diski;
        $this->save();
    }
}
