<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $orders = Order::where('status', 'new')->orderBy('created_at', 'desc')->get();

        return view('admin.index', compact('orders'));
    }

    public function accept($id)
    {
        $order = Order::findOrFail($id);
        $order->status = 'completed';
        $order->save();

        return back();
    }

    public function refuse($id)
    {
        $order = Order::findOrFail($id);
        $order->status = 'canceled';
        $order->save();

        return back();
    }

    public function completed()
    {
        $orders = Order::where('status', 'completed')->orderBy('updated_at')->get();

        return view('admin.completed', compact('orders'));
    }

    public function canceled()
    {
        $orders = Order::where('status', 'canceled')->orderBy('updated_at')->get();

        return view('admin.canceled', compact('orders'));
    }
}
