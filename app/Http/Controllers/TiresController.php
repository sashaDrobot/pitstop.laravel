<?php

namespace App\Http\Controllers;

use App\Tires;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class TiresController extends Controller
{
    public function index(Request $request)
    {
        $this->validate($request, [
            'price_from' => 'sometimes|nullable|numeric|min:' . Tires::min('end_rozn_price'),
            'price_to' => 'sometimes|nullable|numeric|gte:price_from|max:' . Tires::max('end_rozn_price'),
            'brand' => 'sometimes|nullable|exists:price_shini,brand',
            'type' => 'sometimes|nullable|exists:price_shini,wehicle_type',
            'height' => 'sometimes|nullable|exists:price_shini,height',
            'width' => 'sometimes|nullable|exists:price_shini,width',
            'diameter' => 'sometimes|nullable|exists:price_shini,diameter',
            'season' => 'sometimes|nullable|exists:price_shini,season',
            'thorns' => 'sometimes|nullable|boolean',
            'run_flat' => 'sometimes|nullable|boolean',
            'count' => 'sometimes|nullable|integer|min:1',
            'order' => 'sometimes|nullable|alpha|string',
        ]);

        $conditions = [];
        $conditionNotRunFlat = [];

        $price_from = $request->price_from ?? null;
        $price_to = $request->price_to ?? null;
        $brand = $request->brand ?? null;
        $height = $request->height ?? null;
        $width = $request->width ?? null;
        $type = $request->type ?? null;
        $diameter = $request->diameter ?? null;
        $season = $request->season ?? null;
        $thorns = $request->thorns ?? null;
        $run_flat = $request->run_flat ?? null;
        $count = $request->count ?? null;
        $order = $request->order ?? 'asc';

        if ($price_from) $conditions[] = ['end_rozn_price', '>=', $price_from];
        if ($price_to) $conditions[] = ['end_rozn_price', '<=', $price_to];
        if ($brand) $conditions[] = ['brand', $brand];
        if ($height) $conditions[] = ['height', $height];
        if ($width) $conditions[] = ['width', $width];
        if ($type) $conditions[] = ['wehicle_type', $type];
        if ($diameter) $conditions[] = ['diameter', $diameter];
        if ($season) $conditions[] = ['season', $season];
        if (!is_null($thorns)) $conditions[] = ['ship', $thorns ? 'шип' : 'нешип'];
        if (!is_null($run_flat)) {
            $run_flat ? $conditions[] = ['reinforced', 'Run Flat'] : $conditionNotRunFlat = function ($query) {
                $query->where('reinforced', '!=', 'Run Flat')->orWhereNull('reinforced');
            };
        }
        if ($count) $conditions[] = ['amount', '>=', $count];

        $tires = Tires::select('product_id', 'end_rozn_price', 'name', 'season', 'photo')
            ->orderBy('end_rozn_price', $order)
            ->where($conditions)
            ->where($conditionNotRunFlat)
            ->paginate(18);

        if ($request->ajax()) {
            return response()->json($tires);
        }

        $props = [
            'brand' => $brand,
            'diameter' => $diameter,
            'width' => $width,
            'height' => $height,
            'season' => $season,
            'thorns' => $thorns,
            'run_flat' => $run_flat,
            'order' => $order,
            'count' => $count
        ];


        return view('tires', ['props' => $props]);
    }

    public function getFilterData()
    {
        $brand = Tires::groupBy('brand')->orderBy('brand')->pluck('brand')->all();
        $type = Tires::groupBy('wehicle_type')->orderBy('wehicle_type')->pluck('wehicle_type')->all();
        $width = Tires::groupBy('width')->orderBy('width')->pluck('width')->all();
        $height = Tires::groupBy('height')->orderBy('height')->pluck('height')->all();
        $diameter = Tires::groupBy('diameter')->orderBy('diameter')->pluck('diameter')->all();
        $minPrice = Tires::min('end_rozn_price');
        $maxPrice = Tires::max('end_rozn_price');

        $data = [
            'brands' => $brand,
            'type' => $type,
            'width' => array_map(function ($width) {
                return floatval($width);
            }, $width),
            'height' => array_map(function ($height) {
                return floatval($height);
            }, $height),
            'diameters' => array_map(function ($diameter) {
                return floatval($diameter);
            }, $diameter),
            'minPrice' => $minPrice,
            'maxPrice' => $maxPrice
        ];

        return response()->json($data, 200, ['Content-Type: application/json'], JSON_UNESCAPED_UNICODE);
    }

    public function open($id)
    {
        $tire = Tires::where('product_id', $id)->firstOrFail();

        return view('tire', compact('tire'));
    }
}
