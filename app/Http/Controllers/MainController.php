<?php

namespace App\Http\Controllers;

use App\Disks;
use App\Selection;
use App\Tires;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function about()
    {
        return view('about');
    }

    public function delivery()
    {
        return view('delivery');
    }

    public function getMainFormData()
    {
        $brandTires = Tires::groupBy('brand')->orderBy('brand')->pluck('brand')->all();
        $widthTires = Tires::groupBy('width')->orderBy('width')->pluck('width')->all();
        $heightTires = Tires::groupBy('height')->orderBy('height')->pluck('height')->all();
        $diameterTires = Tires::groupBy('diameter')->orderBy('diameter')->pluck('diameter')->all();

        $diameterDisks = Disks::groupBy('diameter')->orderBy('diameter')->pluck('diameter')->all();
        $pcdDisks = Disks::groupBy('pcd')->orderBy('pcd')->pluck('pcd')->all();
        $etDisks = Disks::groupBy('et')->orderBy('et')->pluck('et')->all();
        $brandDisks = Disks::groupBy('brand')->orderBy('brand')->pluck('brand')->all();
        $diaDisks = Disks::groupBy('dia')->orderBy('dia')->pluck('dia')->all();

        $auto = Selection::groupBy('vendor')->orderBy('vendor')->pluck('vendor')->all();

        $data = [
            'tires' => [
                'brands' => $brandTires,
                'width' => array_map(function ($width){
                    return floatval($width);
                }, $widthTires),
                'height' => array_map(function ($height){
                    return floatval($height);
                }, $heightTires),
                'diameters' => array_map(function ($diameter){
                    return floatval($diameter);
                }, $diameterTires)
            ],
            'disks' => [
                'diameters' => array_map(function ($diameter){
                    return floatval($diameter);
                }, $diameterDisks),
                'pcd' => $pcdDisks,
                'et' => array_map(function ($et){
                    return floatval($et);
                }, $etDisks),
                'brands' => $brandDisks,
                'dia' => array_map(function ($dia){
                    return floatval($dia);
                }, $diaDisks)
            ],
            'auto' => $auto
        ];

        return response()->json($data, 200, ['Content-Type: application/json'], JSON_UNESCAPED_UNICODE);
    }

    public function getModelCar(Request $request)
    {
        $this->validate($request, [
            'vendor' => 'required|filled|exists:podbor_shini_i_diski'
        ]);

        $model = Selection::select('car')->distinct()
            ->orderBy('car')
            ->where('vendor', $request->vendor)
            ->pluck('car')->all();

        $response = [
            'model' => $model
        ];

        return response()->json($response, 200, ['Content-Type: application/json'], JSON_UNESCAPED_UNICODE);
    }

    public function getYearCar(Request $request)
    {
        $this->validate($request, [
            'vendor' => 'required|filled|exists:podbor_shini_i_diski',
            'model' => 'required|filled|exists:podbor_shini_i_diski,car',
        ]);

        $year = Selection::select('year')->distinct()
            ->where('vendor', $request->vendor)
            ->where('car', $request->model)
            ->orderBy('year')
            ->pluck('year')->all();

        $response = [
            'year' => $year
        ];

        return response()->json($response, 200, ['Content-Type: application/json'], JSON_UNESCAPED_UNICODE);
    }

    public function getModificationCar(Request $request)
    {
        $this->validate($request, [
            'vendor' => 'required|filled|exists:podbor_shini_i_diski',
            'model' => 'required|filled|exists:podbor_shini_i_diski,car',
            'year' => 'required|filled|exists:podbor_shini_i_diski,year',
        ]);

        $modification = Selection::select('modification')->distinct()
            ->where('vendor', $request->vendor)
            ->where('car', $request->model)
            ->where('year', $request->year)
            ->orderBy('modification')
            ->pluck('modification')->all();

        $response = [
            'modification' => $modification
        ];

        return response()->json($response, 200, ['Content-Type: application/json'], JSON_UNESCAPED_UNICODE);
    }

    public function getTiresByBrand(Request $request)
    {
        $this->validate($request, [
            'vendor' => 'required|filled|exists:podbor_shini_i_diski,vendor',
            'model' => 'required|filled|exists:podbor_shini_i_diski,car',
            'year' => 'required|filled|exists:podbor_shini_i_diski,year',
            'modification' => 'required|filled|exists:podbor_shini_i_diski,modification',
        ]);

        $tires = Selection::select('zavod_shini', 'zamen_shini', 'tuning_shini')
            ->where('vendor', $request->vendor)
            ->where('car', $request->model)
            ->where('year', $request->year)
            ->where('modification', $request->modification)->firstOrFail();

        $response = [
            'tires' => $tires
        ];

        return response()->json($response, 200, ['Content-Type: application/json'], JSON_UNESCAPED_UNICODE);
    }

    public function getDisksByBrand(Request $request)
    {
        $this->validate($request, [
            'vendor' => 'required|filled|exists:podbor_shini_i_diski,vendor',
            'model' => 'required|filled|exists:podbor_shini_i_diski,car',
            'year' => 'required|filled|exists:podbor_shini_i_diski,year',
            'modification' => 'required|filled|exists:podbor_shini_i_diski,modification',
        ]);

        $disks = Selection::select('gaika', 'pcd', 'diametr', 'zavod_diskov', 'zamen_diskov', 'tuning_diski')
            ->where('vendor', $request->vendor)
            ->where('car', $request->model)
            ->where('year', $request->year)
            ->where('modification', $request->modification)->firstOrFail();

        $response = [
            'disks' => $disks
        ];

        return response()->json($response, 200, ['Content-Type: application/json'], JSON_UNESCAPED_UNICODE);
    }
}