<?php

namespace App\Http\Controllers;

use App\Disks;
use Illuminate\Http\Request;

class DisksController extends Controller
{
    public function index(Request $request)
    {
        $this->validate($request, [
            'price_from' => 'sometimes|nullable|numeric|min:' . Disks::min('end_rozn_price'),
            'price_to' => 'sometimes|nullable|numeric|gte:price_from|max:' . Disks::max('end_rozn_price'),
            'brand' => 'sometimes|nullable|exists:price_diski,brand',
            'type' => 'sometimes|nullable|exists:price_diski,wheel_type',
            'diameter' => 'sometimes|nullable|exists:price_diski,diameter',
            'width' => 'sometimes|nullable|exists:price_diski,width',
            'dia' => 'sometimes|nullable',
            'from_et' => 'sometimes|nullable|exists:price_diski,et',
            'to_et' => 'sometimes|nullable|exists:price_diski,et|gt:' . floatval($request->from_et),
            'pcd' => 'sometimes|nullable|string|max:50',
            'et' => 'sometimes|nullable|exists:price_diski,et',
            'count' => 'sometimes|nullable|integer|min:1',
            'order' => 'sometimes|nullable|alpha|string',
        ]);

        $conditions = [];

        $price_from = $request->price_from ?? null;
        $price_to = $request->price_to ?? null;
        $brand = $request->brand ?? null;
        $type = $request->type ?? null;
        $diameter = $request->diameter ?? null;
        $width = $request->width ?? null;
        $dia = $request->dia ?? null;
        $from_et = $request->from_et ?? null;
        $to_et = $request->to_et ?? null;
        $et = $request->et ?? null;
        $pcd = $request->pcd ?? null;
        $count = $request->count ?? null;
        $order = $request->order ?? 'asc';

        if ($price_from) $conditions[] = ['end_rozn_price', '>=', $price_from];
        if ($price_to) $conditions[] = ['end_rozn_price', '<=', $price_to];
        if ($brand) $conditions[] = ['brand', $brand];
        if ($type) $conditions[] = ['wheel_type', $type];
        if ($diameter) $conditions[] = ['diameter', $diameter];
        if ($width) $conditions[] = ['width', $width];
        if ($dia) $conditions[] = ['dia', $dia];
        if ($from_et) $conditions[] = ['et', '>=', $from_et];
        if ($to_et) $conditions[] = ['et', '<=', $to_et];
        if ($et) $conditions[] = ['et', $et];
        if ($pcd) $conditions[] = ['pcd', $pcd];
        if ($count) $conditions[] = ['amount', '>=', $count];

        $disks = Disks::select('product_id', 'end_rozn_price', 'name', 'photo')
            ->where($conditions)
            ->orderBy('end_rozn_price', $order)
            ->paginate(18);

        if ($request->ajax()) {
            return response()->json($disks);
        }

        $toEtList = $from_et ? array_map(function ($et) {
            return floatval($et);
        }, Disks::where('et', '>', $from_et)->groupBy('et')->orderBy('et')->pluck('et')->all()) : null;

        $props = [
            'brand' => $brand,
            'diameter' => $diameter,
            'width' => floatval($width),
            'dia' => $dia,
            'from_et' => $from_et,
            'toEtList' => $toEtList,
            'to_et' => $to_et,
            'pcd' => $pcd,
            'order' => $order,
            'count' => $count
        ];

        return view('disks', ['props' => $props]);
    }

    public function getFilterData()
    {
        $diameter = Disks::groupBy('diameter')->orderBy('diameter')->pluck('diameter')->all();
        $pcd = Disks::groupBy('pcd')->orderBy('pcd')->pluck('pcd')->all();
        $et = Disks::groupBy('et')->orderBy('et')->pluck('et')->all();
        $brand = Disks::groupBy('brand')->orderBy('brand')->pluck('brand')->all();
        $dia = Disks::groupBy('dia')->orderBy('dia')->pluck('dia')->all();
        $width = Disks::groupBy('width')->orderBy('width')->pluck('width')->all();
        $minPrice = Disks::min('end_rozn_price');
        $maxPrice = Disks::max('end_rozn_price');

        $data = [
            'diameters' => array_map(function ($diameter) {
                return floatval($diameter);
            }, $diameter),
            'pcd' => $pcd,
            'et' => array_map(function ($et) {
                return floatval($et);
            }, $et),
            'dia' => array_map(function ($dia) {
                return floatval($dia);
            }, $dia),
            'width' => array_map(function ($width) {
                return floatval($width);
            }, $width),
            'brands' => $brand,
            'minPrice' => $minPrice,
            'maxPrice' => $maxPrice
        ];

        return response()->json($data, 200, ['Content-Type: application/json'], JSON_UNESCAPED_UNICODE);
    }

    public function open($id)
    {
        $disk = Disks::where('product_id', $id)->firstOrFail();

        return view('disk', compact('disk'));
    }
}
