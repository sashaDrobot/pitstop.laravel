<?php

namespace App\Http\Controllers;

use App\Disks;
use App\Mail\SendOrder;
use App\Order;
use App\Tires;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class BasketController extends Controller
{
    public function index()
    {
        $idDisks = Disks::select('product_id')->get()->toArray();
        $idTires = Tires::select('product_id')->get()->toArray();

        $data = [];

        foreach ($idDisks as $id) {
            if (isset($_COOKIE[$id['product_id']])) {
                $disk = Disks::select('product_id', 'end_rozn_price', 'name', 'photo')->where('product_id', $id)->firstOrFail();
                $disk->count = $_COOKIE[$id['product_id']];
                $data[] = $disk;
            }
        }

        foreach ($idTires as $id) {
            if (isset($_COOKIE[$id['product_id']])) {
                $tire = Tires::select('product_id', 'end_rozn_price', 'name', 'photo')->where('product_id', $id)->firstOrFail();
                $tire->count = $_COOKIE[$id['product_id']];
                $data[] = $tire;
            }
        }

        return view('basket', compact('data'));
    }

    public function order(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:300',
            'email' => 'sometimes|nullable|email|max:100',
            'phone' => 'required|string|max:25',
            'city' => 'required|string|max:100',
            'delivery' => 'required|string|max:50',
            'address' => 'required|string|max:300',
            'payment' => 'required|string|max:50',
            'products' => 'required|array|min:1',
            'products.*' => 'required'
        ]);

        $order = new Order();

        $order->name = $request->name;
        $order->email = $request->email;
        $order->phone = $request->phone;
        $order->city = $request->city;
        $order->delivery = $request->delivery;
        $order->address = $request->address;
        $order->payment = $request->payment;
        $order->products = $request->products;

        $order->save();

        Mail::to('PitStopMDA97779@gmail.com')
            ->send(new SendOrder($order));

        foreach($_COOKIE as $key => $value) setcookie($key, '', time() - 3600, '/');

        return response()->json('Success', 200);
    }

    public function count()
    {
        $idDisks = Disks::select('product_id')->get()->toArray();
        $idTires = Tires::select('product_id')->get()->toArray();

        $count = 0;

        foreach ($idDisks as $id) {
            if (isset($_COOKIE[$id['product_id']])) {
               $count++;
            }
        }

        foreach ($idTires as $id) {
            if (isset($_COOKIE[$id['product_id']])) {
               $count++;
            }
        }

        return $count;
    }
}
