<?php

namespace App\Console\Commands;

use App\Disks;
use App\Tires;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class Reload extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reload data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('init:download');

        Disks::truncate();
        $this->info('Table Disks successfully cleared');
        Tires::truncate();
        $this->info('Table Tires successfully cleared');

        $this->call('init:db', ['file-name' => 'price_diski.csv']);
        $this->call('init:db', ['file-name' => 'price_shini_gruz.xls']);
        $this->call('init:db', ['file-name' => 'price_shini_legk.xls']);

        Log::info('Reload database successful!');
    }
}
