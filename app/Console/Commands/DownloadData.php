<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DownloadData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'init:download';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download data from tyretrader';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Start downloading data from tyretrader.ua");

        $this->info("Generating url");
        $disksUrl = "http://opt.wheelshop.com.ua/api/csv/wheels/1.0/?email=Pitstopmda97779@gmail.com&passwd=48zd3a";
    //    $tiresUrl = "http://opt.wheelshop.com.ua/api/csv/tires/1.0/?email=Pitstopmda97779@gmail.com&passwd=48zd3a";

        $disksFileName = "price_diski.csv";
  //      $tiresFileName = "price_shini.csv";

       // $disksUrl = "https://tyretrader.ua/download_price.php?code=f0e5ca8152499af2f77d8f5930a4cb111&type=xls";
        $tiresGruzUrl = "https://tyretrader.ua/download_price.php?code=73464fcb2d68f396efa15b2cefdfd7ce2&type=xls";
        $tiresLegkUrl = "https://tyretrader.ua/download_price.php?code=5a907d9bbd6cc554919b92e9d5dac5302&type=xls";

     //   $disksFileName = "price_diski.xls";
        $tiresGruzFileName = "price_shini_gruz.xls";
        $tiresLegkFileName = "price_shini_legk.xls";

        file_put_contents("public/storage/$disksFileName", fopen($disksUrl, 'r'));
        $this->info("Save $disksFileName");
    //    file_put_contents("public/storage/$tiresFileName", fopen($tiresUrl, 'r'));
    //    $this->info("Save $tiresFileName");

        file_put_contents("public/storage/$tiresGruzFileName", fopen($tiresGruzUrl, 'r'));
        $this->info("Save $tiresGruzFileName");
        file_put_contents("public/storage/$tiresLegkFileName", fopen($tiresLegkUrl, 'r'));
        $this->info("Save $tiresLegkFileName");

        $this->info("Files successfully downloaded.");
    }
}
