<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Init extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialisation application';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('init:download');
        $this->call('migrate:refresh');
        $this->call('init:db', ['file-name' => 'podbor.xlsx']);
        $this->call('init:db', ['file-name' => 'price_diski.csv']);
        $this->call('init:db', ['file-name' => 'price_shini_gruz.xls']);
        $this->call('init:db', ['file-name' => 'price_shini_legk.xls']);
    }
}
