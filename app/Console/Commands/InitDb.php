<?php

namespace App\Console\Commands;

use App\Disks;
use App\Selection;
use App\Tires;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class InitDb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'init:db {file-name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read xls-file and filling database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '-1');

        $file = $this->argument('file-name');

        if ($file == 'podbor.xlsx' || $file == 'price_diski.csv' || $file == 'price_shini_gruz.xls' || $file == 'price_shini_legk.xls') {

            $this->info("Start downloading data from $file");
            $path = Storage::disk('public')->path($file);

            if ($file == 'podbor.xlsx') {
                $path = public_path($file);
                $data = Excel::load($path, function ($reader) {})->get();
                $bar = $this->output->createProgressBar(count($data));
                foreach ($data as $key => $value) {
                    $item = new Selection();
                    $item->insertSelection($value);
                    $bar->advance();
                }
                $bar->finish();
                $this->info("$file is successful");
            } elseif ($file == 'price_diski.csv') {
                $data = Excel::load($path, 'Windows-1251', function ($reader) {})->get();
                $bar = $this->output->createProgressBar(count($data));
                foreach ($data as $key => $value) {
                    $item = new Disks();
                    $item->insertDisks(str_getcsv($value, ';'));
                    $bar->advance();
                }
                $bar->finish();
                $this->info("$file is successful");
            } elseif ($file == 'price_shini_gruz.xls' || $file == 'price_shini_legk.xls') {
                $data = Excel::load($path, function ($reader) {})->get();
                $bar = $this->output->createProgressBar(count($data));
                foreach ($data as $key => $value) {
                    $item = new Tires();
                    $item->insertTires($value);
                    $bar->advance();
                }
                $bar->finish();
                $this->info("$file is successful");
            }
            $this->info('Database is completed successful.');
        } else {
            $this->error('Not command');
        }
    }
}
