<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Disks extends Model
{
    protected $table = 'price_diski';
    public $timestamps = false;

    public function insertDisks($value)
    {
        $this->name = explode('":"', $value[0])[1] . " " . $value[1] . " " .  floatval(str_replace(',', '.', $value[4])) . "x" . floatval(str_replace(',', '.', $value[3])) . " " . str_replace(',', '.', $value[5]) . " ET" . floatval($value[6]);
        $this->brand = str_replace('&', ' and ', explode('":"', $value[0])[1]);
        $this->model = $value[1];
        $this->color = $value[2];
        $this->diameter = str_replace(',', '.', $value[3]);
        $this->width = str_replace(',', '.', $value[4]);
        $this->pcd = str_replace(',', '.', $value[5]);
        $this->et = $value[6] ? str_replace(',', '.', $value[6]) : null;
        $this->dia = $value[7] ? str_replace(',', '.', $value[7]) : null;
        $this->amount = $value[8];
        $this->end_rozn_price = str_replace(',', '.', $value[9]);
        $this->end_opt_price = str_replace(',', '.', $value[10]);
        $this->photo = str_replace('"}', '', $value[11]);
        $this->save();
    }
}
