<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      //  $this->call(PodborShiniIDiskiTableSeeder::class);
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'alex.drobot22@gmail.com',
            'password' => bcrypt('admin1111'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
