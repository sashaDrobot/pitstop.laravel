<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriceShiniTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_shini', function (Blueprint $table) {
            $table->integer('product_id');
            $table->string('brand');
            $table->decimal('height')->nullable();
            $table->decimal('end_opt_price');
            $table->decimal('end_rozn_price');
            $table->integer('year')->nullable();
            $table->string('supplier_city');
            $table->string('date');
            $table->decimal('diameter');
            $table->string('load_index')->nullable();
            $table->string('speed_index')->nullable();
            $table->string('model');
            $table->string('name');
            $table->string('ini_opt_price');
            $table->integer('amount');
            $table->string('supplier');
            $table->string('ini_rozn_price');
            $table->string('season');
            $table->string('country')->nullable();
            $table->string('wehicle_type');
            $table->string('reinforced')->nullable();
            $table->string('photo')->nullable();
            $table->string('ship')->nullable();
            $table->decimal('width');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_shini');
    }
}
