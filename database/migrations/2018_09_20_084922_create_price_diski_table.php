<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriceDiskiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_diski', function (Blueprint $table) {
            $table->increments('product_id');
            $table->string('name');
            $table->string('pcd');
            $table->string('brand');
            $table->decimal('et')->nullable();
            $table->decimal('end_opt_price');
            $table->decimal('end_rozn_price');
            $table->decimal('diameter');
            $table->decimal('dia')->nullable();
            $table->string('model');
            $table->integer('amount');
            $table->string('photo')->nullable();
            $table->string('color')->nullable();
            $table->decimal('width');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_diski');
    }
}
