<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePodborShiniIDiskiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('podbor_shini_i_diski', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('vendor', 300);
			$table->string('car', 300);
			$table->string('year', 300);
			$table->string('modification', 300);
			$table->string('pcd', 300)->nullable();
			$table->string('diametr', 300)->nullable();
			$table->string('gaika', 300)->nullable();
			$table->string('zavod_shini', 300)->nullable();
			$table->string('zamen_shini', 300)->nullable();
			$table->string('tuning_shini', 300)->nullable();
			$table->string('zavod_diskov', 300)->nullable();
			$table->string('zamen_diskov', 300)->nullable();
			$table->string('tuning_diski', 300)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('podbor_shini_i_diski');
	}

}
